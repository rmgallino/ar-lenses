using TMPro;
using UnityEngine;

public class SetIOR : MonoBehaviour
{
    private MeshRenderer objectRenderer;
    void Start()
    {
        objectRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        // Update screen pixels to object screen position
        Vector2 screenPixels = Camera.main.WorldToScreenPoint(transform.position);
        screenPixels = new Vector2(screenPixels.x / Screen.width, screenPixels.y / Screen.height);
        objectRenderer.material.SetVector("_ObjectScreenPosition", screenPixels);
    }

    public void ChangeIORValue(float value)
    {
        objectRenderer.material.SetFloat("_IOR", value);
    }

    public void ChangeZoomAmount(float value)
    {
        objectRenderer.material.SetFloat("_Zoom", value);

    }
}
